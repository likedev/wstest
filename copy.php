<?php
## 
# Small script for deploying the code to our machines
##

system("tar -cvf /tmp/wstest.tar ../wstest && gzip /tmp/wstest.tar --force");

# 2 copy tarball to all machines and remove old files
$hosts = file("/etc/dsh/machines.list");
system("dsh -a -c 'rm -rf /home/ubuntu/wstest*'");

foreach($hosts as $key => $host) {
	$host=trim($host);
	system("scp /tmp/wstest.tar.gz $host:/home/ubuntu/");
	system("ssh $host 'cd /home/ubuntu/ && tar -xvf wstest.tar.gz'");
	echo ($key+1)." of ". count($hosts) . " has their files\r\n";
}
# finally uncompress it 
system("dsh -a -c 'cd /home/ubuntu/ && tar -xvf wstest.tar.gz'");
