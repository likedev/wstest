node index.js 1000 realtime.uat.like.tv/primus/websocket

 - Starta ett gäng ec2 instanser med publika ipn
 - apt-get install dsh
 - lägg ipn till alla instanser i /etc/dsh/machines.list
 - registrera nyckelfilen med ssh-add <sök väg till dev_platform.pem>
 - testa allt med dsh -a -M -c "uptime"
 - Kopiera ws-lasttest applikationen med copy.php
 - Kör dsh -a -m -i -c "cd /home/ubuntu/websocket_loadtest; node index.js 20 500 realtime.uat.like.tv/primus/websocket" för att skapa 20 forkar med 500 sockets var på varje instans. 
 - lsof -a -p <pid> för att se antalet öppna fildeskriptorer för processen som testas.
 - kolla /proc/<pid>/limits för att se hur många nofile som är tillgängliga
 - Testet failar för att antalet fildeskriptorer nåtts så ändra i /etc/security/limits
    * soft nofile 65000 # Ger "processen" möjlighet att öppna 65000 filer upp till hard limit
    * hard nofile 65000 # Ger alla processer möjlighet att öppna 65000 filer
    echo "session required pam_limits.so" >> /etc/pam.d/login # detta ser till att filbegrånsningarna appliceras vid login shell.
    fs.file-max = 100000 i /etc/sysctl.conf
 - Öka tcp minne
    cat /proc/sys/net/ipv4/tcp_mem ger tre värden, low threshold memory limit, memory pressure mode limit, max memory limit. Allt är i pages och måste multipliceras med getconf PAGESIZE (4096 oftast)

    cat /proc/net/sockstat om tcp: mem * PAGESIZE går över max memory limit har instansen nått max connection limit.
    för att öka, sysctl -a |grep tcp. Leta upp tcp_mem, ta de tre värdena och gångra med 4. Skriv sedan ex. sudo sysctl net.ipv4.tcp_mem="58716 78296 117432" för att öka tcp_mem size

 - bra commando: sysctl -a |grep tcp

 - Sätt upp nytt subnät inom vpc, ha det som 7/16
 - Skapa ny route tabell för subnätet med 0.0.0.0/0 och VPCets gateway
