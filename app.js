'use strict';

var chalk = require('chalk'),
	cluster = require('cluster'),
	numForks = 0, numSocks = 0,
	x = 0,
	async = require('async');

if (!process.argv[2] || !process.argv[3] || !process.argv[4]) {
	console.log(chalk.red("Need number of forks, socket per fork and url example:"));
	console.log("\tnode app.js <forks> <sockets> <url> <wslibrary>");
	console.log("\tnode app.js 10 50 localhost:7000 ws will open total 10 forks with 50 connections each, 500 connections in total using ws library");
	console.log("\tThe ws library is very fast for websockets but sockjs can be used. sockjs usually keeps the connections open longer, but slower...");
	process.exit();
}

numForks = process.argv[2];
numSocks = process.argv[3];

console.log(chalk.magenta("Setting up master. Connecting to: " + process.argv[4] + ". Forking: " + numForks + " times with each fork holding: " + numSocks + " sockets."));

cluster.setupMaster({
	exec : "client.js",
	silent : false
});

cluster.on('listening', function (worker, address) {
	console.log(chalk.magenta("Master: A worker is now connected to " + address.address + ":" + address.port));
});


cluster.on('online', function (worker) {
	console.log(chalk.magenta("Master: A fork responded"));
});

async.whilst(
	function () {
		var la = (x < numForks);
		return la;
	},
	function (cb) {
		x++;
		cluster.fork({num: x});
		cb();
	},
	function (err) {
		if (err) console.log(err, x);
		console.log("Started all workers");
	}
);