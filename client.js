'use strict';

var count,
	chalk = require('chalk'),
	forkNumber = process.env.num,
	async = require('async'),
	Primus = require('primus'),
	Socket = Primus.createSocket({ transformer: "sockjs", parser: "JSON" }),
	webSocket = require('ws');

/**
 * Constructor
 * @param numberOfSockets - the amount of socket this fork should handle
 * @param url - to connect to
 */
var socketHolder = function (numberOfSockets, url, library) {
	this.url = url;
	this.sockets = [];
	this.connectedSockets = 0;
	this.disconnectedSockets = 0;
	this.numberOfSockets = numberOfSockets;
	this.library = library;
	this.message = {action: "subscribe", type: "match", key: "se123456"};
};

socketHolder.prototype.write = function (socketNumber, message) {
	console.log(chalk.green("Fork: " + forkNumber), ", ", chalk.yellow("Socket: " + socketNumber), chalk.cyan(": " + message));
};

socketHolder.prototype.createWebsocket = function () {
	var self = this,
		socket;

	if (self.library === 'sockjs')
		socket = new Socket('http://' + self.url);
	else
		socket = new webSocket("ws://" + self.url);

	// Here it is a bit messy, this should if this is used later be refactored...
	socket.on('open', function () {
		self.connectedSockets++;
		self.write(socket.socketNumber, "Opened connection. Total connected sockets: " + self.connectedSockets + ", totalDisconnects: " + self.disconnectedSockets);

		// its a bit different to send data depending on library. Here we go for longpolling
		if (self.library === 'sockjs') {
			socket.write(self.message);
			self.write(socket.socketNumber, "Sent subscription: " + JSON.stringify(self.message));

			socket.on('error', function (message) {
				self.write(socket.socketNumber, "Got error: " + message);
				self.disconnectedSockets--;
			});
			socket.on('close', function (message) {
				self.write(socket.socketNumber, "Closed connection: " + message);
				self.disconnectedSockets--;
				self.createWebsocket();
			});

			socket.on('data', function (data, flags) {
				self.write(socket.socketNumber, "Got data: " + JSON.stringify(data));
			});
		} else { // Default to websocket

			socket.on('error', function (message) {
				self.write(socket.socketNumber, "Got error: " + message);
				self.disconnectedSockets--;
			});

			socket.on('close', function (message) {
				self.write(socket.socketNumber, "Closed connection: " + message);
				self.disconnectedSockets--;
				self.createWebsocket();
			});

			socket.send(JSON.stringify(self.message), function (err) {
				if (err) self.write(socket.socketNumber, "Got error when sending: " + err);
				else {
					self.write(socket.socketNumber, "Sent subscription.", JSON.stringify(self.message));
				}
			});

			socket.on('message', function (data, flags) {
				self.write(socket.socketNumber, "Got data: " + JSON.stringify(data));
			});
		}
	});
};

socketHolder.prototype.init = function (callback) {
	var socket, count = 0, self = this;

	for (count = 0; count < self.numberOfSockets; count++) {
		//this.sockets[count].socketNumber = count;
		this.createWebsocket();
	}
};

/**
 * Sends message to all sockets
 *
 * @param message
 */
socketHolder.prototype.broadcast = function (message) {
	var self = this;

	async.each(
	self.sockets,
	function (socket, callback) {
		socket.send(JSON.stringify(message), function (err) {
			if (err) callback(self.write(socket.socketNumber, "Got error when sending: " + err));
			else {
				self.write(socket.socketNumber, "Sent subscription.", JSON.stringify(message));
				callback();
			}
		});
	},
	function (err) {
		if (err) console.log(err);
	}
	);
};

// fuck this for now... Should have socket disconnects later...
socketHolder.prototype.exit = function () {

};

socketHolder.prototype.start = function () {
	var self = this, msg = {action: "subscribe", type: "match", key: "se123456"};
	this.write("All", "Should create: " + this.numberOfSockets + " sockets");
	this.init();
};

// Main
var sh = new socketHolder(process.argv[3], process.argv[4], process.argv[5]);
sh.start();
